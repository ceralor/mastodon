# frozen_string_literal: true

class TextFormatter
  include ActionView::Helpers::TextHelper
  include ERB::Util
  include RoutingHelper

  URL_PREFIX_REGEX = /\A(https?:\/\/(www\.)?|xmpp:)/.freeze

  DEFAULT_REL = %w(nofollow noopener noreferrer).freeze

  DEFAULT_OPTIONS = {
    multiline: true,
  }.freeze

  attr_reader :text, :options

  # @param [String] text
  # @param [Hash] options
  # @option options [Boolean] :multiline
  # @option options [Boolean] :with_domains
  # @option options [Boolean] :with_rel_me
  # @option options [Array<Account>] :preloaded_accounts
  def initialize(text, options = {})
    @text    = text
    @options = DEFAULT_OPTIONS.merge(options)
  end

  def entities
    @entities ||= Extractor.extract_entities_with_indices(text, extract_url_without_protocol: false)
  end

  def to_s
    return ''.html_safe if text.blank?

    html = rewrite do |entity|
      if entity[:url]
        link_to_url(entity)
      elsif entity[:hashtag]
        link_to_hashtag(entity)
      elsif entity[:screen_name]
        link_to_mention(entity)
      end
    end

    html = simple_format(html, {}, sanitize: false).delete("\n") if multiline?

    html.html_safe # rubocop:disable Rails/OutputSafety
  end

  private

  def rewrite
    entities.sort_by! do |entity|
      entity[:indices].first
    end

    result = ''.dup

    last_index = entities.reduce(0) do |index, entity|
      indices = entity[:indices]
      result << h(text[index...indices.first])
      result << yield(entity)
      indices.last
    end

    result << h(text[last_index..-1])

    result
  end

  def link_to_url(entity)
    url = Addressable::URI.parse(entity[:url]).to_s
    rel = with_rel_me? ? (DEFAULT_REL + %w(me)) : DEFAULT_REL

    prefix      = url.match(URL_PREFIX_REGEX).to_s
    display_url = url[prefix.length, 30]
    suffix      = url[prefix.length + 30..-1]
    cutoff      = url[prefix.length..-1].length > 30

    <<~HTML.squish
      <a href="#{h(url)}" target="_blank" rel="#{rel.join(' ')}"><span class="invisible">#{h(prefix)}</span><span class="#{cutoff ? 'ellipsis' : ''}">#{h(display_url)}</span><span class="invisible">#{h(suffix)}</span></a>
    HTML
  rescue Addressable::URI::InvalidURIError, IDN::Idna::IdnaError
    h(entity[:url])
  end

  def link_to_hashtag(entity)
    hashtag = entity[:hashtag]
    url     = tag_url(hashtag)

    <<~HTML.squish
      <a href="#{h(url)}" class="mention hashtag" rel="tag">#<span>#{h(hashtag)}</span></a>
    HTML
  end

  def link_to_mention(entity)
    username, domain = entity[:screen_name].split('@')
    domain           = nil if local_domain?(domain)
    account          = nil

    case domain
    when 'twitter.com'
      return link_to_twitter(username)
    when 'tumblr.com'
      return link_to_tumblr(username)
    when 'weasyl.com'
      return link_to_weasyl(username)
    when 'furaffinity.net'
      return link_to_furaffinity(username)
    when 'furrynetwork.com', 'beta.furrynetwork.com'
      return link_to_furrynetwork(username)
    when 'sofurry.com'
      return link_to_sofurry(username)
    when 'inkbunny.net'
      return link_to_inkbunny(username)
    when 'e621.net'
      return link_to_e621(username)
    when 'e926.net'
      return link_to_e926(username)
    when 'f-list.net'
      return link_to_flist(username)
    when 'deviantart.com'
      return link_to_deviantart(username)
    when 'artstation.com'
      return link_to_artstation(username)
    when 'github.com'
      return link_to_github(username)
    when 'gitlab.com'
      return link_to_gitlab(username)
    when 'bitbucket.org'
      return link_to_bitbucket(username)
    when 'telegram.org'
      return link_to_telegram(username)
    when 'picarto.tv'
      return link_to_picarto(username)
    when 'twitch.tv'
      return link_to_twitch(username)
    when 'reddit.com'
      return link_to_reddit(username)
    when 'poizen.me'
      return link_to_poizen(username)
    when 'patreon.com'
      return link_to_patreon(username)
    when 'boosty.to'
      return link_to_boosty(username)
    when 'subscribestar.adult'
      return link_to_subscribestar(username)
    end

    if preloaded_accounts?
      same_username_hits = 0

      preloaded_accounts.each do |other_account|
        same_username = other_account.username.casecmp(username).zero?
        same_domain   = other_account.domain.nil? ? domain.nil? : other_account.domain.casecmp(domain)&.zero?

        if same_username && !same_domain
          same_username_hits += 1
        elsif same_username && same_domain
          account = other_account
        end
      end
    else
      account = entity_cache.mention(username, domain)
    end

    return "@#{h(entity[:screen_name])}" if account.nil?

    url = ActivityPub::TagManager.instance.url_for(account)
    display_username = same_username_hits&.positive? || with_domains? ? account.pretty_acct : account.username

    <<~HTML.squish
      <span class="h-card"><a href="#{h(url)}" class="u-url mention">@<span>#{h(display_username)}</span></a></span>
    HTML
  end

  def link_to_twitter(username)
    link_to(username, "https://twitter.com/#{username}", "twitter.com")
  end

  def link_to_tumblr(username)
    link_to(username, "https://#{username}.tumblr.com", "tumblr.com")
  end

  def link_to_weasyl(username)
    link_to username "https://weasyl.com/~#{username}" "weasyl.com"
  end

  def link_to_furaffinity(username)
    link_to(username, "https://furaffinity.net/user/#{username}", "furaffinity.net")
  end

  def link_to_furrynetwork(username)
    link_to(username, "https://furrynetwork.com/#{username}", "furrynetwork.com")
  end

  def link_to_inkbunny(username)
    link_to(username, "https://inkbunny.net/#{username}", "inkbunny.net")
  end

  def link_to_sofurry(username)
    link_to(username, "https://#{username}.sofurry.com", "sofurry.com")
  end

  def link_to_e621(username)
    link_to username "https://e621.net/user/show/#{username}" "e621.net"
  end

  def link_to_e926(username)
    link_to username "https://e926.net/user/show/#{username}" "e926.net"
  end

  def link_to_flist(username)
    link_to username "https://f-list.net/c/#{username}" "f-list.net"
  end

  def link_to_deviantart(username)
    link_to(username, "https://#{username}.deviantart.com", "deviantart.com")
  end

  def link_to_artstation(username)
    link_to(username, "https://www.artstation.com/#{username}", "artstation.com")
  end

  def link_to_github(username)
    link_to(username, "https://github.com/#{username}", "github.com")
  end

  def link_to_gitlab(username)
    link_to(username, "https://gitlab.com/#{username}", "gitlab.com")
  end

  def link_to_bitbucket(username)
    link_to(username, "https://bitbucket.org/#{username}", "bitbucket.org")
  end

  def link_to_telegram(username)
    link_to(username, "https://t.me/#{username}", "telegram.org")
  end

  def link_to_picarto(username)
    link_to(username, "https://picarto.tv/#{username}", "picarto.tv")
  end

  def link_to_twitch(username)
    link_to(username, "https://twitch.tv/#{username}", "twitch.tv")
  end

  def link_to_reddit(username)
    link_to(username, "https://reddit.com/u/#{username}", "reddit.com")
  end

  def link_to_poizen(username)
    link_to(username, "https://poizen.me/#{username}", "poizen.me")
  end

  def link_to_patreon(username)
    link_to(username, "https://www.patreon.com/#{username}", "patreon.com")
  end

  def link_to_boosty(username)
    link_to(username, "https://boosty.to/#{username}", "boosty.to")
  end

  def link_to_subscribestar(username)
    link_to(username, "https://subscribestar.adult/#{username}", "subscribestar.adult")
  end

  def link_to(username, url, domain)
    "<span class=\"h-card\"><a href=\"#{url}\" target=\"blank\" rel=\"noopener noreferrer\" class=\"u-url mention\">@<span>#{username}@#{domain}</span></a></span>"
  end

  def entity_cache
    @entity_cache ||= EntityCache.instance
  end

  def tag_manager
    @tag_manager ||= TagManager.instance
  end

  delegate :local_domain?, to: :tag_manager

  def multiline?
    options[:multiline]
  end

  def with_domains?
    options[:with_domains]
  end

  def with_rel_me?
    options[:with_rel_me]
  end

  def preloaded_accounts
    options[:preloaded_accounts]
  end

  def preloaded_accounts?
    preloaded_accounts.present?
  end
end
