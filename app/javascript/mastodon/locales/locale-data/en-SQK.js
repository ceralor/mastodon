/*eslint eqeqeq: "off"*/
/*eslint no-nested-ternary: "off"*/
/*eslint quotes: "off"*/

import en from "react-intl/locale-data/en.js";

export default [
    ...en,
    { locale: "en-SQK", parentLocale: "en" }
];
